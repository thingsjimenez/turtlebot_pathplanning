**Turtlebot 2 - Path planning**

*You can [watch this video](https://www.youtube.com/watch?v=xSdHlC2ISq8) , It is something similar that we want to achieve with different trajectory planning algorithms with the turtlebot 2 mobile platform.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.
